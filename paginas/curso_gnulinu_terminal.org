#+TITLE: Introducción a Gnu/Linux: la terminal, comandos básicos y creación de scripts.

** Presentación

Este curso pretende entregar los conocimientos básicos para lograr /fluidez/ en
la comprensión, composición de comandos y creación  de *scripts* utilizando las
herramientas base disponibles desde la /terminal/.

La /terminal/ es una aplicación central dentro de los sistemas operativos de
computadora, también llamada /línea de comandos/ (CLI). Permite realizar
operaciones computacionales como la gestión de *directorios y archivos*, así
como también procesar datos en forma de /texto/.

Las y los estudiantes que participen concienzudamente del curso podrán
aprehender los herramientas base para poder trabajar con *computadoras*. Será el
inicio de un camino en el entendimiento y creación de herramientas tecnológicas
libres y comunitarias.

** Contenidos esenciales

Los contenidos base y esenciales consideran lo siguiente:

- Lógica proposicional :: Bases lógicas para computar sentencias
- Teoría de conjuntos :: Bases matemáticas de operación computacional
- Comandos básicos de la terminal :: Directorios y archivos
- Comandos básicos de la terminal :: Acceso a dispositivos
- Búsqueda sobre textos :: Uso del comando *grep*
- Edición de textos :: Uso del comando *sed*
- Expresiones regulares de texto :: Reconocimiento y declaración de *regex*
- /Strings/ :: Manipulación de cadenas de texto
- /Datetime/ :: Manipulación de datos de fecha-tiempo
- /Awk/ básico :: Lenguaje para procesar texto
- /Awk/ avanzado :: Aplicaciones avanzadas para procesar texto
- /Scripts/ con argumentos :: Archivos de texto con comandos ordenados secuenciales
- Paralelización :: Uso intensivo de los recursos con grandes cantidades de datos
- Gráficos con /GnuPlot/ :: Graficar datos con GnuPlot
- Aplicaciones con datos geográficos :: Procesar geodata con comandos
- /SSH/, terminal de acceso remoto :: Trabajo remoto y uso de terminales 
- Control de versiones con /Git/ (básico) :: Gestión de versiones

** Herramientas necesarias

- Computador portátil :: Puede ser uno viejito desde el año 2008 (100GB disco duro, 2GB Ram, mínimo).
- Sistema Operativo /GNU/Linux/ :: /Debian/, /LinuxMint/ o /Ubuntu/. Se instalará en una jornada previa a comenzar las clases la distribución /Debian/ 10.
- Cuaderno para apuntes y lápices :: Los apuntes son lo mas importante

** Referencias

Ver documento: [[https://www.mediafire.com/file/9jgqmhi45lp2tw7/391180922-Software-Libre-desde-lo-Comunitario_%281%29.pdf/file][Software Libre desde lo Comunitario]]

