from django import template

register = template.Library()


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.simple_tag
def gettime(seconds):
    hour = 60*60
    day = 60*60*24
    if seconds > 60 and seconds < hour:
        return "%.2f [min]" % (seconds/60)
    elif seconds >= hour and seconds < day:
        return "%.2f [hr]" % (seconds/hour)
    elif seconds >= day:
        return "%.2f [dias]" % (seconds/day)
    else:
        return "%.2f [s]" % (seconds)



@register.simple_tag
def call_method(obj, method_name, *args):
    method = getattr(obj, method_name, None)
    if method:
        return method(*args)
    else:
        return None

    
