from haystack import indexes
from .models import Localizacion

class LocalizacionIndex(indexes.SearchIndex, indexes.Indexable):
    """
    ubicacion = models.CharField(max_length=200,help_text="calle y nro", verbose_name='Ubicación')
    comuna = models.ForeignKey(Comuna)
    geo_point = models.PointField(blank = True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    """
    text = indexes.CharField(document = True, use_template= True)
    ubicacion = indexes.CharField(model_attr='ubicacion')
    localizacion = indexes.LocationField(model_attr='geo_point')

    def get_model(self):
        return Localizacion

    def index_queryset(self, using=None):
        """A usar cuando el índice esta actualizado"""
        return self.get_model().objects.all()