from django.contrib import admin
from .models import Localizacion, Comuna, Region
# Register your models here.
admin.site.register(Region)
admin.site.register(Comuna)
admin.site.register(Localizacion)