from django.conf.urls import url
from django.urls import path
from .views import (ApplicationView, ContenidosCursoView, SesionCursoView)
app_name = "contenido"

urlpatterns = [
    path('', ApplicationView.as_view(), name='application'),
    path('curso=<slug:curso>/generacion=<slug:generacion>',
         ContenidosCursoView.as_view(),
         name='curso_main'),
    path(
        'curso=<slug:curso>/generacion=<slug:generacion>/sesion=<int:posicion>',
        SesionCursoView.as_view(),
        name='sesion_main')
]
