from django.shortcuts import render
from django.views.generic import FormView
from django.views.generic import ListView
from django.views.generic import DetailView

from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import SearchContentForm
from .models import (ContenidosProyecto, Sesion, Contenido)
from apps.proyecto.models import Proyecto
from django.shortcuts import get_object_or_404


def app_menu():
    menu = {}
    return menu


class ContenidoBase:
    app_active = 'Contenido'

    def menu(self):
        this_menu = app_menu()
        return this_menu


# Create your views here.
# TODO: add ListView -> post aggregar modelo
class ApplicationView(LoginRequiredMixin, ContenidoBase, FormView):
    """
    Formulario para buscar contenidos dentro de las sesiones
    - Busca en cada curso que la persona tiene acceso
    - Busca coincidencias en títulos, tags y contenidos?
    - Los resultados los muestra aquí mismo en una tabla de resulados
    """
    template_name = 'contenido/app.dj.html'
    form_class = SearchContentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["menu"] = self.menu()
        context["menu_title"] = self.app_active.capitalize()
        return context


class ContenidosCursoView(LoginRequiredMixin, ContenidoBase, DetailView):
    """
    En base al curso-generacion
    Encuentra 'ContenidosCurso'
    Dado que es una relación 1:1
    """
    template_name = 'contenido/main.dj.html'
    model = ContenidosProyecto
    slug_field = 'proyecto'
    slug_url_kwarg = 'generacion'

    def get(self, request, *args, **kwargs):
        self.url_kwargs = kwargs
        return super().get(request, *args, **kwargs)

    def get_object(self, **kwargs):
        generacion = self.url_kwargs.get('generacion')
        self.proyecto = Proyecto.objects.get(slug_generacion=generacion)
        self.object = get_object_or_404(ContenidosProyecto,
                                        proyecto=self.proyecto)
        return self.object

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["menu"] = self.menu()
        context["menu_title"] = self.app_active.capitalize()
        context["proyecto"] = self.proyecto
        context["curso"] = self.proyecto.curso
        context["menu"] = self.menu()
        context["bread"] = "general"
        return context


class SesionCursoView(LoginRequiredMixin, ContenidoBase, DetailView):
    template_name = 'contenido/sesion.dj.html'
    model = Sesion
    slug_field = 'proyecto'
    slug_url_kwarg = 'generacion'

    def get(self, request, *args, **kwargs):
        self.url_kwargs = kwargs
        return super().get(request, *args, **kwargs)

    def get_object(self, **kwargs):
        generacion = self.url_kwargs.get('generacion')
        posicion = self.url_kwargs.get('posicion')
        self.proyecto = Proyecto.objects.get(slug_generacion=generacion)
        contenidos = ContenidosProyecto.objects.get(proyecto=self.proyecto)
        self.contenidos = contenidos
        sesion = Sesion.objects.get(posicion=posicion,
                                    contenidos_proyecto=contenidos)
        return sesion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["menu"] = self.menu()
        context["menu_title"] = self.app_active.capitalize()
        context["proyecto"] = self.proyecto
        context["curso"] = self.proyecto.curso
        context["menu"] = self.menu()
        context["clase"] = self.object.contenidos.filter(tipo="CL")
        context["ejercicio"] = self.object.contenidos.filter(tipo="EJ")
        context["bread"] = "sesion"
        context["siguiente"] = None
        context["anterior"] = None
        if self.object.posicion < self.contenidos.cantidad_clases:
            context["siguiente"] = Sesion.objects.get(
                contenidos_proyecto=self.contenidos,
                posicion=self.object.posicion + 1)
        if self.object.posicion > 1:
            context["anterior"] = Sesion.objects.get(
                contenidos_proyecto=self.contenidos,
                posicion=self.object.posicion - 1)
        return context
