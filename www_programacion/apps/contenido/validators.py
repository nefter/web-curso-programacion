import os
from django.core.exceptions import ValidationError


def validate_file_extension(value, exts=["csv"]):
    ext = value.name.split(".")[-1]
    if not ext.lower() in exts:
        raise ValidationError(f"Extensión no admitida, {ext}, solo {exts}")
