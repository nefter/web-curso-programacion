from celery import shared_task
from .send import webmail_send
import time
import re


@shared_task
def send_mail(mail_fields, template, template_info, debug=False):
    try:
        result = 0
        intentos = 10
        count = 0
        while result == 0 and count <= intentos:
            result = webmail_send(mail_fields, template, template_info, debug)
            if result:
                return {"status": True, "result": result, "mail": mail_fields}
            time.sleep(1)
            count += 1
        return {"status": False, "result": result, "mail": mail_fields}
    except Exception as e:
        print("Error al enviar", e)
        return {
            "status": False,
            "result": 0,
            "mail": mail_fields,
            "error": str(e)
        }
