from apps.proyecto.functions import submit_active_projects
import json
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from files.functions.links import LinkData
from apps.home.views import menu_nav
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
from networktools.library import my_random_string
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from apps.inscripcion.models import Inscripcion, ChequeoCodigo
from apps.proyecto.models import Proyecto, Curso
from apps.inscripcion.forms import InscripcionForm
from django.views.generic.edit import FormView, UpdateView, CreateView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.utils.timezone import datetime, timedelta
from .forms import InscripcionPostulanteForm
from .models import RevisionInscripcion
from django.http import JsonResponse
from django.core.paginator import Paginator
from apps.proyecto.models import get_member
from groups_manager.models import (Group, Member, GroupEntity, GroupMember,
                                   GroupMemberRole, GroupType)

# from django.contrib.messages.views import SuccessMessageMixin
"""

Inscripciones Activas

"""


def my_cursos(member):
    group_memberships = member.group_membership.all()
    managers = [
        "Creador", "Responsable", "Profesor", "Ayudante", "Administración"
    ]
    GrupoProyecto = GroupType.objects.get_or_create(label='Curso')[0]
    gt_managers = list(
        map(lambda name: GroupType.objects.get_or_create(label=name)[0],
            managers))
    select_groups = list(
        filter(lambda gm: gm.group.group_type in gt_managers,
               group_memberships))
    parent_groups = set(filter(lambda g: g.group.parent, select_groups))
    grupos_cursos = set(
        filter(lambda g: g.group.parent.group_type == GrupoProyecto,
               parent_groups))
    cursos = list({g.group.parent.curso for g in grupos_cursos})
    return cursos


def my_proyectos(member):
    group_memberships = member.group_membership.all()
    managers = [
        "Creador", "Responsable", "Profesor", "Ayudante", "Administración"
    ]
    GrupoProyecto = GroupType.objects.get_or_create(label='Proyecto')[0]
    gt_managers = list(
        map(lambda name: GroupType.objects.get_or_create(label=name)[0],
            managers))
    select_groups = list(
        filter(lambda gm: gm.group.group_type in gt_managers,
               group_memberships))
    parent_groups = set(filter(lambda g: g.group.parent, select_groups))
    grupos_proyectos = set(
        filter(lambda g: g.group.parent.group_type == GrupoProyecto,
               parent_groups))
    proyectos = list({g.group.parent.proyecto for g in grupos_proyectos})
    return proyectos


class InscripcionesActivasView(LoginRequiredMixin, ListView):
    """
    TODO:
    Listar proyectos de curso activos para inscripción
    Configurar alertas:
    AZUL-VERDE-AMARILLO-ROJO
    >1mes,1mes,2sem,1sem
    """
    template_name = 'inscripcion/listar_activas.dj.html'
    model = Inscripcion
    today_min = datetime.today()

    def get_context_data(self, *args, **kwargs):
        today_min = self.today_min
        kwargs = super().get_context_data(**kwargs)
        proyectos_inscripcion_activa = submit_active_projects()
        kwargs['inscripciones_activas'] = proyectos_inscripcion_activa
        kwargs.update({'menu_nav': menu_nav()})
        return kwargs


"""
Inscripciones Pasadas
"""


class InscripcionesPasadasView(ListView):
    pass


"""
Creating the view functions to show the form
"""


class InscripcionView(LoginRequiredMixin, FormView):
    """
    Not use loginrequired because show a message to register if not login

    TODO: Controlar que no se vuelva a inscribir si ya existe
    """
    template_name = 'inscripcion/inscripcion.dj.html'
    form_class = InscripcionForm
    sucess_url = '/success/'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)  # for python 3

    def get(self, request, *args, **kwargs):
        self.url_kwargs = kwargs
        self.msg = ""
        return self.render_to_response(self.get_context_data(**kwargs))


#    def post(self, request, *args, **kwargs):
#        self.url_kwargs = kwargs
#        return self.render_to_response(self.get_context_data(**kwargs))

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        curso_name = self.url_kwargs.get('slug_curso')
        generacion_name = self.url_kwargs.get('slug_generacion')
        form = kwargs.get('form')
        proyecto_data = Proyecto.objects.get(slug_generacion=generacion_name)
        curso_data = proyecto_data.curso.slug_nombre
        self.proyecto = proyecto_data
        self.random = my_random_string(string_length=10)
        codigo = ChequeoCodigo(valor_aleatorio=self.random)
        codigo.save()
        is_valid = kwargs.get('is_valid', 100)
        form.fields["proyecto"].initial = proyecto_data.id
        form.fields["valor_aleatorio"].initial = self.random
        form.fields["usuario"].initial = self.request.user.id
        msg = "Todo bien"
        show_msg = "hide_msg"
        if not is_valid:
            form.fields['representa_organizacion'].initial = False
            msg = "Revisa bien que todos los campos tengan información"
            show_msg = "show_msg"
        show_of = "true" if kwargs.get('repr_org') else "false"
        inscripciones = self.request.user.inscripciones.filter(
            proyecto=self.proyecto)
        if not self.msg:
            self.msg = ""
        kwargs.update({
            'curso_data': curso_data,
            'proyecto': proyecto_data,
            'valor_aleatorio': self.random,
            'show_org_fields': show_of,
            'show_msg': show_msg,
            "inscripciones": inscripciones,
            "debug": settings.DEBUG,
            "msg": self.msg if self.msg else ""
        })
        kwargs.update(
            {'background_image_curso': proyecto_data.imagen_fondo.url})
        kwargs.update({'menu_nav': menu_nav()})
        return kwargs

    def form_valid(self, form):
        fields = ('subject', 'body', 'from', 'to')
        cd = form.cleaned_data
        usuario = cd.get("usuario")
        representa_org = cd.get('representa_organizacion')
        instance = form.save()
        fields_dict = dict(
            zip(fields, (
                '¡Inscripción a Curso de Programación realizada!',
                "La inscripcion al curso ha sido exitosa, muchas gracias.",
                'cursodeprogramacion@disroot.org',
                [usuario.email if usuario.email else "dpineda@uchile.cl"],
            )))
        user_kwargs = {'fields': fields_dict}
        user_kwargs.update({
            'nombre': f"{instance.proyecto.curso} | {instance.proyecto}",
            'nombre_completo': instance.nombre_completo,
            'fecha_inicio': instance.proyecto.fecha_inicio,
            'fecha_final': instance.proyecto.fecha_final
        })
        form.send_usuario_email(**user_kwargs)
        admin_fields_dict = dict(
            zip(fields, (
                '¡Inscripción a Curso de Programación realizada!',
                "Se ha inscrito una nueva persona",
                'cursodeprogramacion@disroot.org',
                settings.ADMIN_EMAIL_LIST,
            )))
        admin_kwargs = {
            'fields': admin_fields_dict,
            'nombre': f"{instance.proyecto.curso} | {instance.proyecto}",
        }
        data_persona = {
            "username": usuario.username,
            "nombre_completo": str(usuario.profile),
            "email": usuario.email,
            "phonenumber": str(usuario.profile.phonenumber)
        }
        admin_kwargs['data_persona'] = data_persona
        form.send_admin_email(**admin_kwargs)
        return HttpResponseRedirect(
            self.get_success_url(id=instance.id,
                                 valor_aleatorio=instance.valor_aleatorio))

    def form_invalid(self,
                     form,
                     is_valid=False,
                     representa_org=False,
                     **kwargs):
        """If the form is invalid, render the invalid form."""
        return self.render_to_response(
            self.get_context_data(form=form,
                                  is_valid=False,
                                  repr_org=representa_org,
                                  **kwargs))

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        self.msg = ""
        self.url_kwargs = kwargs
        form = self.get_form()
        generacion_name = self.url_kwargs.get('slug_generacion')
        proyecto_data = Proyecto.objects.get(slug_generacion=generacion_name)
        user = request.user
        inscripciones = request.user.inscripciones.filter(
            proyecto=proyecto_data)
        if form.is_valid() and not inscripciones:
            return self.form_valid(form)
        else:
            if inscripciones:
                self.msg = "¡Ya estás inscrito!"
            return self.form_invalid(form)

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('inscripcion:exitosa',
                                kwargs={
                                    'id':
                                    kwargs.get('id', 1),
                                    'valor_aleatorio':
                                    kwargs.get('valor_aleatorio')
                                })
        else:

            return reverse_lazy('inscripcion:exitosa',
                                kargs={
                                    'id':
                                    1,
                                    'valor_aleatorio':
                                    kwargs.get('valor_aleatorio')
                                })


"""
Una vez que se completa exitosamente el formulario se muestra un mensaje
"""


class InscripcionExitosa(LoginRequiredMixin, TemplateView):
    template_name = 'inscripcion/exitosa.dj.html'

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        valor_aleatorio = kwargs.get('valor_aleatorio')
        if valor_aleatorio == valor_aleatorio:
            inscripcion = Inscripcion.objects.filter(
                valor_aleatorio=valor_aleatorio).first()
            kwargs.update({'inscripcion': inscripcion, 'mostrar': True})
        kwargs.update({'menu_nav': menu_nav()})
        return kwargs


"""
Inscripcion app
"""


def app_inscripcion_menu():
    menu = {
        "Proyecto": [
            LinkData("Todos", "inscripcion:list_projects_all"),
            LinkData("Activos",
                     "inscripcion:list_projects",
                     opts={"kwargs": {
                         "choice": "activos"
                     }}),
            LinkData("En curso",
                     "inscripcion:list_projects",
                     opts={"kwargs": {
                         "choice": "en_curso"
                     }}),
            LinkData("Pasados",
                     "inscripcion:list_projects",
                     opts={"kwargs": {
                         "choice": "pasados"
                     }}),
        ],
        "Inscripción": [
            LinkData("Todas", "inscripcion:list_inscribed_all"),
            LinkData("Pendientes",
                     "inscripcion:list_inscribed",
                     opts={"kwargs": {
                         "choice": "en_revision"
                     }}),
            LinkData("Aprobados",
                     "inscripcion:list_inscribed",
                     opts={"kwargs": {
                         "choice": "aprobadas"
                     }}),
            LinkData("Rachazados",
                     "inscripcion:list_inscribed",
                     opts={"kwargs": {
                         "choice": "rechazadas"
                     }}),
        ],
    }
    return menu


class InscriptionBase:
    app_active = "Inscripcion"

    def menu(self):
        this_menu = app_inscripcion_menu()
        return this_menu


class InscriptionAppView(InscriptionBase, TemplateView):
    """
    Show general landscape
    """
    template_name = 'inscripcion/app.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        kwargs["menu"] = self.menu()
        kwargs["menu_nav"] = menu_nav()
        return kwargs


class InscriptionProjectsView(InscriptionBase, ListView):
    """
    Get list of projects and show by inscriptions
    listview
    """
    model = Proyecto
    template_name = 'inscripcion/por_proyecto.dj.html'
    paginate_by = 20
    choice = "Todos"
    app_active = "Proyecto"
    order_by = '-fecha_inicio_inscripcion'

    def get(self, request, *args, **kwargs):
        self.params = kwargs
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        today = datetime.today().date()
        queryset = super().get_queryset()
        self.queryset = queryset
        # filter by url kwargs
        choice = self.params.get('choice', "all")
        user = self.request.user
        member = get_member(user)
        proyectos = my_proyectos(member)
        cursos = my_cursos(member)
        if choice != "all":
            if choice == 'activos':
                queryset = queryset.filter(fecha_inscripcion_inicio__lte=today,
                                           fecha_inscripcion_final__gte=today,
                                           usuario=user)
                if proyectos:
                    queryset |= queryset.filter(
                        fecha_inscripcion_inicio__lte=today,
                        fecha_inscripcion_final__gte=today,
                        proyecto__in=proyectos)
                self.choice = "Activos"
            elif choice == 'en_curso':
                queryset = queryset.filter(fecha_inicio__lte=today,
                                           fecha_final__gte=today,
                                           usuario=user)
                if proyectos:
                    queryset |= queryset.filter(fecha_inicio__lte=today,
                                                fecha_final__gte=today,
                                                proyecto__in=proyectos)
                self.choice = "En curso"
            elif choice == 'pasados':
                queryset = queryset.filter(fecha_final__lt=today, usuario=user)
                if proyectos:
                    queryset |= queryset.filter(fecha_final__lt=today,
                                                proyecto__in=proyectos)
                self.choice = "Pasados"
            else:
                queyset = Proyecto.objects.none()
        elif choice == "all":
            limit_date = today - timedelta(days=365)
            queryset = queryset.filter(
                fecha_inscripcion_inicio__gte=limit_date)
        queryset.order_by(self.order_by)
        return queryset

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        kwargs["menu"] = self.menu()
        kwargs["menu_nav"] = menu_nav()
        kwargs["choice"] = self.choice
        kwargs["app_active"] = self.app_active
        return kwargs


class InscriptionProjectView(InscriptionBase, DetailView):
    """
    Manage inscriptions in one project
    can allow, sellect all in a set
    """
    model = Proyecto
    template_name = 'inscripcion/admin_proyecto.dj.html'
    slug_field = 'slug_generacion'
    slug_url_kwarg = 'slug_generacion'
    app_active = "Proyecto"

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        kwargs["menu"] = self.menu()
        kwargs["menu_nav"] = menu_nav()
        kwargs["app_active"] = self.app_active
        kwargs["cifras"] = {
            "on_review": self.object.inscripciones.filter(status="on_review"),
            "accepted": self.object.inscripciones.filter(status="accepted"),
            "rejected": self.object.inscripciones.filter(status="rejected"),
            "total": self.object.inscripciones.count()
        }
        return kwargs


class InscriptionInscribedView(InscriptionBase, ListView):
    model = Inscripcion
    template_name = 'inscripcion/por_inscripcion.dj.html'
    paginate_by = 20
    choice = "Todas"
    app_active = "Inscripción"
    order_by = '-fecha_inscripcion'

    def get(self, request, *args, **kwargs):
        self.params = kwargs
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        today = datetime.today().date()
        queryset = super().get_queryset()
        self.queryset = queryset
        # filter by url kwargs
        choice = self.params.get('choice', "all")
        user = self.request.user
        member = get_member(user)
        proyectos = my_proyectos(member)
        if choice != "all":
            if choice == 'aprobadas':
                """
                Mostrar las inscripciones que hasta hoy
                se pueden cambiar
                """
                queryset = queryset.filter(usuario=user, status='accepted')
                if proyectos:
                    queryset |= Inscripcion.objects.filter(
                        proyecto__in=proyectos, status='accepted')
                self.choice = "Activas"
            elif choice == 'en_revision':
                """
                Mostrar aquellas inscripciones que hasta la fecha se pueden
                aprobar-rechazar
                """
                # para el mismo usuario
                queryset = queryset.filter(usuario=user, status='on_review')
                # si se es admin de algun protecto
                if proyectos:
                    queryset |= Inscripcion.objects.filter(
                        proyecto__in=proyectos, status='on_review')
                    print("Resultado,", queryset)
                self.choice = "Revisión"
            elif choice == 'rechazadas':
                """
                Inscripciones rechazadas por usuario
                """
                queryset = queryset.filter(usuario=user, status='rejected')
                if proyectos:
                    queryset |= Inscripcion.objects.filter(
                        proyecto__in=proyectos, status='rejected')
                print("Queryset", queryset)
                self.choice = "Rechazadas"
            else:
                queyset = Inscripcion.objects.none()
        elif choice == "all":
            limit_date = today - timedelta(days=365)
            queryset = queryset.filter(fecha_inscripcion__gte=limit_date)
        queryset.order_by(self.order_by)
        return queryset

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        kwargs["menu"] = self.menu()
        kwargs["menu_nav"] = menu_nav()
        kwargs["choice"] = self.choice
        kwargs["app_active"] = self.app_active
        kwargs["cifras"] = {
            "on_review": self.queryset.filter(status="on_review"),
            "accepted": self.queryset.filter(status="accepted"),
            "rejected": self.queryset.filter(status="rejected"),
            "total": len(self.queryset)
        }
        return kwargs


class InscripcionPostulanteView(InscriptionBase, LoginRequiredMixin,
                                CreateView):
    template_name = 'inscripcion/postulante.dj.html'
    form_class = InscripcionPostulanteForm
    model = RevisionInscripcion
    success_url = reverse_lazy("inscripcion:list_inscribed_all")

    def form_valid(self, form):
        fields = ('subject', 'body', 'from', 'to')
        cd = form.cleaned_data
        # revision inscripcion
        instance = form.save()
        instance.inscripcion.status = cd.get('status')
        instance.inscripcion.save()
        # get user inscribed
        if instance.inscripcion.status == 'accepted':
            form.add_user_to_group()
        if instance.inscripcion.status in {"accepted", "rejected"}:
            form.send_user_email()
            form.send_admins_email()
        return HttpResponseRedirect(self.success_url)

    def get_initial(self):
        initial = super().get_initial()
        id_inscripcion = self.kwargs.get('pk')
        self.inscripcion = Inscripcion.objects.get(pk=id_inscripcion)
        initial["inscripcion"] = self.inscripcion.id
        initial["user"] = self.request.user.id
        return initial

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args, **kwargs)
        kwargs["menu"] = self.menu()
        kwargs["menu_nav"] = menu_nav()
        kwargs["app_active"] = self.app_active
        kwargs["inscripcion"] = self.inscripcion
        return kwargs


def ajax_preview_email(request, *args, **kwargs):
    """
    Ajax to take msg texto and return http response
    """
    email_preview = "NO-PREVIEW"
    template_name = "proyecto/emails/preview.dj.html"
    tipos = {"rejected": "rechazo", "accepted": "acepta"}
    if request.is_ajax():
        data_dict = json.loads(request.body)
        inscripcion_id = data_dict.get("inscripcion")
        mensaje = data_dict.get("mensaje_particular", "SIN-MENSAJE")
        tipo_origin = data_dict.get("tipo", "rechazo")
        tipo = tipos.get(tipo_origin, "rechazo")
        inscripcion = Inscripcion.objects.get(id=inscripcion_id)
        mensaje_template = inscripcion.proyecto.email_templates
        fn = getattr(mensaje_template, f"load_template_{tipo}")
        email_context = {"mensaje_particular": mensaje}
        if fn:
            email_preview = fn(**email_context)
            context = {
                "email_html": email_preview,
                "title": f"Mensaje Preview {inscripcion.proyecto} | {tipo}"
            }
            return JsonResponse(context)
    return JsonResponse(email_preview)
