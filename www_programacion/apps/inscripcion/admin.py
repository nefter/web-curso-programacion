from django.contrib import admin
from apps.inscripcion.models import (Inscripcion)
# Register your models here.
#from markdownx.admin import MarkdownxModelAdmin


class InscripcionAdmin(admin.ModelAdmin):
    list_display = ("persona", 'proyecto', 'fecha_inscripcion')


admin.site.register(Inscripcion, InscripcionAdmin)
