from django.views.generic import DetailView
from django.views.generic.edit import FormView


class InscripcionView(FormView):
    template_name = 'inscripcion/inscripcion.dj.html'
    form_class = InscripcionForm


class InscripcionExitosa(DetailView):
    model = Inscripcion
    template_name = 'inscripcion/exitosa.dj.html'
