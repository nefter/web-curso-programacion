from django.conf.urls import url
from django.urls import path

from apps.inscripcion import views

app_name = 'inscripcion'

urlpatterns = [
    # app view
    path("", views.InscriptionAppView.as_view(), name='application'),
    # logit-auth
    # manage inscripcion by projects
    # activo, en_curso, pasado
    path("proyectos",
         views.InscriptionProjectsView.as_view(),
         name='list_projects_all'),
    path("proyectos/seleccion=<str:choice>",
         views.InscriptionProjectsView.as_view(),
         name='list_projects'),
    path("proyecto/administrar=<slug:slug_generacion>",
         views.InscriptionProjectView.as_view(),
         name='project_admin'),
    # manage inscripcion by projects
    # choices: todas, activas, revision, rechazadas
    path("inscritos",
         views.InscriptionInscribedView.as_view(),
         name='list_inscribed_all'),
    path("inscritos/seleccion=<str:choice>",
         views.InscriptionInscribedView.as_view(),
         name='list_inscribed'),
    # logit-auth
    # manage inscripcion by projects
    path("proyecto/name=<slug:slug_curso>/gen=<slug:slug_generation>",
         views.InscriptionProjectView.as_view(),
         name='person_project'),
    # logit-auth
    path('activas', views.InscripcionesActivasView.as_view(), name='activas'),
    # login-auth
    path('pasadas', views.InscripcionesPasadasView.as_view(), name='pasadas'),
    # normal user
    path('<slug:slug_curso>/<slug:slug_generacion>',
         views.InscripcionView.as_view(),
         name='persona'),
    # normal user
    path('exitosa/<int:id>/<str:valor_aleatorio>',
         views.InscripcionExitosa.as_view(),
         name='exitosa'),
    path("postulante/num=<int:pk>",
         views.InscripcionPostulanteView.as_view(),
         name='postulante'),
    path("ajax/preview/email",
         views.ajax_preview_email,
         name="preview_postulante")
]
