from django.conf.urls import url
from django.urls import path
from .views import (ApplicationView, CreateGroupView, EditGroupView,
                    DeleteGroupView, DeleteGroupOkView, GroupView,
                    GroupsListView, UsersListView, EditUserWithGroupView,
                    AddMemberToGroupCourseView, AddMemberToGroupProjectView,
                    EditMemberToGroupCourseView, EditMemberToGroupProjectView,
                    RemoveMemberGroupView)
app_name = 'admin_group'

urlpatterns = [
    path('app', ApplicationView.as_view(), name='application'),
    path('grupo/crear', CreateGroupView.as_view(), name='create-group'),
    path('grupo/editar/nombre=<str:slug_name>',
         EditGroupView.as_view(),
         name='edit-group'),
    path('grupo/borrar/nombre=<str:slug_name>',
         DeleteGroupView.as_view(),
         name='delete-group'),
    path('grupo/borrar/ok=<str:slug_name>',
         DeleteGroupOkView.as_view(),
         name='delete-ok-group'),
    path('grupo/nombre=<str:slug_name>', GroupView.as_view(), name='group'),
    path('lista/grupos', GroupsListView.as_view(), name='list-groups'),
    path('lista/usuarios', UsersListView.as_view(), name='list-users'),
    path('edita/usuario', EditUserWithGroupView.as_view(), name='asociate'),
    path('miembro/agregar/grupo_curso=<int:pk>',
         AddMemberToGroupCourseView.as_view(),
         name='member2group_course'),
    path('editar/miembro=<int:member_id>/grupo_curso=<int:pk>',
         EditMemberToGroupCourseView.as_view(),
         name='edit_member2group_course'),
    path('miembro/agregar/grupo_proyecto=<int:pk>',
         AddMemberToGroupProjectView.as_view(),
         name='member2group_project'),
    path('editar/miembro=<int:member_id>/grupo_proyecto=<int:pk>',
         EditMemberToGroupProjectView.as_view(),
         name='edit_member2group_project'),
    path(
        'remover/miembro=<int:member_id>/grupo=[<slug:group_type>,<int:group_id>]',
        RemoveMemberGroupView.as_view(),
        name="remove_member")
]
