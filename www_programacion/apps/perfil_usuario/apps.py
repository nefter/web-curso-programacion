from django.apps import AppConfig


class PerfilUsuarioConfig(AppConfig):
    name = 'perfil_usuario'
