from django.contrib import admin
from .models import AccesoUsuario, MensajeUsuario, AdjuntoMensaje
# Register your models here.
admin.site.register(AccesoUsuario)
admin.site.register(MensajeUsuario)
admin.site.register(AdjuntoMensaje)
