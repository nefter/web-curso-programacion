const marked = require('marked');
import "regenerator-runtime/runtime.js";
const createDOMPurify = require('dompurify');
const { JSDOM } = require('jsdom');
import hljs from 'highlight.js';
import 'highlight.js/styles/github.css';
const window = new JSDOM('').window;
const DOMPurify = createDOMPurify(window);
const renderer = new marked.Renderer();
import katex from 'katex';
import renderMathInElement from "katex/dist/contrib/auto-render.mjs";
import 'katex/dist/katex.css';

function showtext(editor, result, marked){
    let html_pre = marked(editor.value);
    let html = html_pre.replace(/<br>/g,'');
    result.innerHTML=html;
    let options = {
        throwOnError:true,
        delimiters:[
        {left: "$$", right: "$$", display: true},
        {left: "\\(", right: "\\)", display: false},
        {left: "\\[", right: "\\]", display: true}],
        colorIsTextColor:true};
    renderMathInElement(result, options);
};

document.addEventListener(
  'DOMContentLoaded',
    () => {
        const editor_id = "editor";
        const result_id = "resultado";
        let editor = document.getElementById(editor_id);
        let result = document.getElementById(result_id);

        const renderer = new marked.Renderer();
        marked.setOptions({
          renderer: renderer,
          highlight: function(code, language) {
            const validLanguage = hljs.getLanguage(language) ? language : 'plaintext';
            return hljs.highlight(validLanguage, code).value;
          },
          pedantic: false,
          gfm: true,
          breaks: true,
            sanitize: false,
            sanitizer: (text)=>{
                 return DOMPurify.sanitize(text);
            },
          smartLists: true,
          smartypants: false,
            xhtml: false,
            headerIds: true,
            headerPrefix: "titulo_"
        });
        editor.oninput = ()=>{
            showtext(editor, result, marked);
        };
        showtext(editor, result, marked);
  },
);

