from django.forms import Textarea


class MarkdownTextAreaWidget(Textarea):
    template_name = 'articulo/widgets/markdown_textarea.dj.html'
    class Media:
        css = {
            'all': ('articulo/widgets/django_marked/dist/index.css',)
        }
        js = ('articulo/widgets/django_marked/dist/index.js',)
