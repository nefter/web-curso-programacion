from django.conf.urls import url
from django.urls import path

from apps.organizacion import views

app_name = 'organizacion'

urlpatterns = [
    path('postulaciones',
         views.PostulacionesLista.as_view(),
         name='postulaciones'),
]
