from django.conf.urls import url
from django.urls import path
# from django.contrib import admin
from apps.contacto.views import ContactoView, contacto_sent_view, status_task_contacto

app_name = 'contacto'

urlpatterns = [
    path('', ContactoView.as_view(), name='formulario'),
    path('exitoso/', contacto_sent_view, name='exitoso'),
    path("ajax/contacto/task=<str:task_id>",
         status_task_contacto,
         name="email-enviado"),
]
