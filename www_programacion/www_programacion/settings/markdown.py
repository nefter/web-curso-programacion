from datetime import datetime
MARKDOWNX_MARKDOWNIFY_FUNCTION = 'markdownx.utils.markdownify'
MARKDOWNX_MARKDOWN_EXTENSIONS = [
    'markdown.extensions.extra'
]

MARKDOWNX_URLS_PATH = '/markdownx/markdownify/'
MARKDOWNX_UPLOAD_URLS_PATH = '/markdownx/upload/'
MARKDOWNX_MEDIA_PATH = datetime.now().strftime('markdownx/%Y/%m/%d')
MARKDOWNX_UPLOAD_MAX_SIZE = 50 * 1024 * 1024
MARKDOWNX_UPLOAD_CONTENT_TYPES = ['image/jpeg', 'image/png', 'image/svg+xml']
MARKDOWNX_IMAGE_MAX_SIZE = {
    'size': (500, 500),
    'quality': 90
}
MARKDOWNX_SVG_JAVASCRIPT_PROTECTION = True
MARKDOWNX_EDITOR_RESIZABLE = True
MARKDOWNX_SERVER_CALL_LATENCY = 500  # milliseconds
