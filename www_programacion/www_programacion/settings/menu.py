def base_link(data):
    items = ("title", "href", "text")
    return dict(zip(items, (data[0:len(items)])))


def menu_nav(**kwargs):
    main_menu = [
        base_link(('Inicio', '/', "Inicio")),
        base_link(('El Curso', '/pagina/el-curso', "El Curso")),
        base_link(('Nosotros', '/pagina/nosotros', "Nosotros")),
        base_link(('Proyectos', '/proyecto/listar', "Proyectos")),
        base_link(('Contacto', '/contacto', "Contacto")),
    ]

    return main_menu


MENU_NAV = menu_nav()
